#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "crc32.h"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        fprintf(stderr, "inappropriate number of parameters\n");
        return 0;
    }
    struct stat info;
    void *addr;
    uint32_t crc_32;
    int f = open(argv[1], O_RDONLY);
    if (f < 0)
    {
        perror(argv[1]);
        return 0;
    }
    stat(argv[1], &info);
    addr = mmap(NULL, info.st_size, PROT_READ, MAP_SHARED, f, 0);

    crc_32 = crc32((uint8_t *) addr, info.st_size);

    printf("%s : crc32 : %d", argv[1], crc_32);

    munmap(addr, info.st_size);
    close(f);

    return 0;
}
