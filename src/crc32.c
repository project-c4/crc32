#include "crc32.h"

uint32_t crc32(const uint8_t data[], size_t data_lenght)
{
    uint32_t crc32 = 0xFFFFFFFFu;
    for (size_t i = 0; i < data_lenght; i++)
    {
        const uint32_t lookup_index = (crc32 ^ data[i]) & 0xFF;
        crc32 = (crc32 >> 8) ^ crc32_tab[lookup_index];
    }
    crc32 ^= 0xFFFFFFFFu;
    return crc32;
}
