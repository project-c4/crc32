TARGET = crc32
CC ?= gcc
FLAGS += -g -Wall -Wextra -Wpedantic -std=c11

TARGET_PATH = bin/
SRC_PATH = src/
OBJ_PATH = obj/
LIB_PATH = lib/
OBJ =  $(wildcard $(OBJ_PATH)*.o)
LIB = $(wildcard $(LIB_PATH)*.a)

export LIBRARY_PATH = $(LIB_PATH)

all: $(TARGET)

$(TARGET) : $(SRC)
	if [ ! -r ./obj/ ]; then mkdir obj;	fi
	if [ ! -r ./lib/ ]; then mkdir lib; fi
	if [ ! -r ./bin/ ]; then mkdir bin; fi

	$(CC) $(FLAGS) -o $(OBJ_PATH)crc32.o -c  $(SRC_PATH)crc32.c
	ar crc $(LIB_PATH)libcrc32.a $(OBJ_PATH)crc32.o
	$(CC) $(FLAGS) -o $(OBJ_PATH)main.o -c $(SRC_PATH)main.c
	$(CC) $(FLAGS) $(OBJ_PATH)main.o -L. -lcrc32 -o $(TARGET_PATH)$(TARGET)

clean:
	if [ -r ./obj/ ]; then rm -rf $(OBJ_PATH); fi
	if [ -r ./lib/ ]; then rm -rf $(LIB_PATH); fi
	if [ -r ./bin/ ]; then rm -rf $(TARGET_PATH); fi
